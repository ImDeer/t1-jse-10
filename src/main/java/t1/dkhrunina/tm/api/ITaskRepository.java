package t1.dkhrunina.tm.api;

import t1.dkhrunina.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void clear();

}