package t1.dkhrunina.tm.api;

import t1.dkhrunina.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void clear();

}