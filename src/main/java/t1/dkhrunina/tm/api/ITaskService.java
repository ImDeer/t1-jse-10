package t1.dkhrunina.tm.api;

import t1.dkhrunina.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}