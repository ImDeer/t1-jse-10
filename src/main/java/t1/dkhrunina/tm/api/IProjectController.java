package t1.dkhrunina.tm.api;

public interface IProjectController {

    void showProjects();

    void createProjects();

    void clearProjects();

}