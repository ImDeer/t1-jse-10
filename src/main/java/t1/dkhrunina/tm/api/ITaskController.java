package t1.dkhrunina.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}