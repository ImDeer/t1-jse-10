package t1.dkhrunina.tm.api;

import t1.dkhrunina.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}