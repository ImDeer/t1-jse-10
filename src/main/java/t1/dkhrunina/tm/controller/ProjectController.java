package t1.dkhrunina.tm.controller;

import t1.dkhrunina.tm.api.IProjectController;
import t1.dkhrunina.tm.api.IProjectService;
import t1.dkhrunina.tm.model.Project;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[Show projects]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createProjects() {
        System.out.println("[Create project]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[Failed to create a project]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[Clear project list]");
        projectService.clear();
        System.out.println("[OK]");
    }

}