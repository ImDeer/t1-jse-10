package t1.dkhrunina.tm.controller;

import t1.dkhrunina.tm.api.ITaskController;
import t1.dkhrunina.tm.api.ITaskService;
import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[Show tasks]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[Create task]");
        System.out.println("Enter name: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[Failed to create a task]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[Clear task list]");
        taskService.clear();
        System.out.println("[OK]");
    }

}