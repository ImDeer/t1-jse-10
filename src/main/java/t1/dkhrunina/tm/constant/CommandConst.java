package t1.dkhrunina.tm.constant;

public final class CommandConst {

    private CommandConst() {
    }

    public static final String HELP = "Help";

    public static final String VERSION = "Version";

    public static final String ABOUT = "About";

    public static final String INFO = "Info";

    public static final String EXIT = "Exit";

    public static final String PROJECT_LIST = "project-list";

    public static final String PROJECT_CREATE = "project-create";

    public static final String PROJECT_CLEAR = "project-clear";

    public static final String TASK_LIST = "task-list";

    public static final String TASK_CREATE = "task-create";

    public static final String TASK_CLEAR = "task-clear";

}